float x = 100;
int y, t = 0;
PImage foto;
import processing.sound.*;
SoundFile file;

void setup(){
   size(800,800);
   background(0);
   foto = loadImage("Disco pare.png");
   file = new SoundFile(this, "Palo.mp3");
   //file.play(); 
}

void draw(){
  image(foto, 400, 400);
  background(205);
  fill(255); //Blanco 255, negro 0
  stroke(0); //Pintar bordes
  
  rect(100, 600, 100, 100); //rectangulo para frenado
  
  rect(600, 600, 100, 100); //rectangulo para acelerador
  
  ellipse(400, 500, 100, 100); //Circulo que se enciende al pisar el freno
  
  //Medidor de velocidad
  line(100, 100, 700, 100); //Parte de arriba
  line(100, 400 ,700, 400); //Parte de abajo
  line(700, 100, 700, 400); //Parte lateral derecha
  line(100, 100, 100, 400); //Parte lateral izquierda
  
  //Lineas de velocidad
  line(150, 100, 150, 150);  //Primera linea  (10 km/h)
  line(200, 100, 200, 200);  //Segunda linea  (20 km/h)
  line(250, 100, 250, 150);  //Tercera linea  (30 km/h)
  line(300, 100, 300, 200);  //Cuarta linea   (40 km/h)  
  line(350, 100, 350, 150);  //Quinta linea   (50 km/h)
  line(400, 100, 400, 200);  //Secta linea    (60 km/h)
  line(450, 100, 450, 150);  //Septima linea  (70 km/h)
  line(500, 100, 500, 200);  //Octava linea   (80 km/h)
  line(550, 100, 550, 150);  //Novena linea   (90 km/h)
  line(600, 100, 600, 200);  //Decima linea   (100 km/h)
  line(650, 100, 650, 150);  //Onceava linea  (110 km/h)
  
  textSize(25);
  fill(0);
  text("0 km/h", 80, 90);
  text("10", 140, 170);
  text("20", 190, 220);
  text("30", 240, 170);
  text("40", 290, 220);
  text("50", 340, 170);
  text("60", 390, 220);
  text("70", 440, 170);
  text("80", 490, 220);
  text("90", 540, 170);
  text("100", 583, 220);
  text("110", 633, 170);
  text("120 km/h", 680, 90);
  
  
  
  //Funcion del freno
  
  if((mouseX > 100) && (mouseY > 600) && (mouseX < 200) && (mouseY < 700) && (x >= 100)){
    fill(255, 0, 0); //Pintado (rellename todo lo que este debajo)
    ellipse(400, 500, 100, 100);
    x = x-1.5;
  }else{
    x = x-0.25;
  }
  
  //Funcion de desacelerar
  
  if((x < 100)){
      x = x+0.999;
    }
    
    if((x > 699)){
      x = x-0.999;
    }
  
  //Funcion de acelerador
  
  if((mouseX > 600) && (mouseY > 600) && (mouseX < 700) && (mouseY < 700) && (x <= 699)){
    x = x+1.1; 
    if (y < 1){
      t = millis();
    }
      y++;
      if (millis() - t >= 10000){       
        image(foto, 300, 400);
        if(!file.isPlaying()){
         file.play(); 
        }
    }
  }else{
    y = 0;
    t = 0;
    file.stop();   //Cosas futuras
  }
 
  //x++; //  x = (x+1)
  println(x, t, millis()-t);
  stroke(255, 0, 0); //Bordes
  line(x, 110, x, 390);
  
  
}
